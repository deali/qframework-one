﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using QFrameworkOne.Diagnostics;

namespace QFrameworkOne {
    /// <summary>
    /// QFramework
    /// </summary>
    public static class QFrameworkStatic {
        private static QDebug Qdb = new QDebug() {CodeLocation = "QFrameworkOne.QFrameworkStatic"};
        private static QComponentVersion v_QFrameworkVersion = new QComponentVersion();

        /// <summary>
        /// 框架版本
        /// </summary>
        public static QComponentVersion Version => v_QFrameworkVersion;

        /// <summary>
        /// 框架版本字符串
        /// </summary>
        public static string VersionStr {
            get { return v_QFrameworkVersion.ToString(); }
        }

        /// <summary>
        /// 静态构造函数
        /// </summary>
        static QFrameworkStatic() {
            Qdb.ConsoleOutput = true;
            //设置版本
            v_QFrameworkVersion.Major = AssemblyVersion.Major;
            v_QFrameworkVersion.Minor = AssemblyVersion.Minor;
            v_QFrameworkVersion.Update = AssemblyVersion.Build;
            v_QFrameworkVersion.Label = QComponentVersionLabel.Alpha;

            Qdb.Log("QFrameworkStatic->QFramework版本" + v_QFrameworkVersion.ToString(), QDebugLogType.Verbose,
                "QFrameworkStatic");
        }

        #region 程序集特性访问器

        /// <summary>
        /// 获取程序集版本信息
        /// </summary>
        public static Version AssemblyVersion => Assembly.GetExecutingAssembly().GetName().Version;

        #endregion
    }
}