﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QFrameworkOne.Diagnostics;

namespace QFrameworkOne.Module {
    /// <summary>
    /// Json简单处理工具，只能处理一层的Json数据
    /// </summary>
    public class JsonHelper : IQMoudule {
        string IQMoudule.Module_Name => "JsonHelper";

        string IQMoudule.Module_Author => "DealiAxy";

        string IQMoudule.Module_Mail => "admin@deali.cn";

        string IQMoudule.Module_Website => "http://blog.deali.cn";

        string IQMoudule.Module_Description => "用于处理简单的Json字符串，解码编码，以及序列化";

        QComponentVersion IQMoudule.Module_Version => new QComponentVersion()
            {Major = 1, Minor = 0, Update = 1, Label = QComponentVersionLabel.Beta};

        QDebug Qdb = new QDebug() {
            DebugCodeLocation = "QFrameworkOne.Module.JsonHelper",
            ConsoleOutput = true,
        };

        /// <summary>
        /// Json处理工具
        /// </summary>
        public JsonHelper() {
        }

        /// <summary>
        /// 将一个键值对应字典结构的数据编码成Json字符串
        /// </summary>
        /// <param name="dict"></param>
        /// <returns></returns>
        public string Encode_Simple(Dictionary<string, string> dict) {
            string ret = "{";
            foreach (KeyValuePair<string, string> p in dict) {
                ret += string.Format("\"{0}\":\"{1}\",", p.Key, p.Value);
            }

            ret = ret.TrimEnd(','); //去掉最后的一个逗号
            ret += "}"; //加上右括号包起来
            return ret;
        }

        /// <summary>
        /// 将单层的Json字符串解码，返回键值对应的字典对象
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public Dictionary<string, string> Decode_Simple(string json) {
            try {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                json = json.Trim(); //先去掉两边的空白
                char[] array = json.ToCharArray();
                if (array[0] != '{')
                    throw new FormatException("Json字符串的格式好像有问题，没有找到左边第一个大括号");
                if (array[array.Length - 1] != '}')
                    throw new FormatException("Json字符串的格式好像有问题，没有找到右边最后一个大括号");

                json = json.TrimStart('{');
                json = json.TrimEnd('}');
                string[] items = json.Split(',');
                foreach (string s in items) {
                    string[] item = s.Split(':');
                    if (item.Length == 2) {
                        item[0] = item[0].Trim('\"');
                        item[1] = item[1].Trim('\"');

                        //如果有多个重名的Key，则以最后一个为准
                        if (!dict.ContainsKey(item[0])) {
                            dict.Add(item[0], item[1]);
                        }
                        else {
                            dict[item[0]] = item[1];
                        }
                    }
                    else {
                        item[0] = item[0].Trim('\"');
                        //如果有多个重名的Key，则以最后一个为准
                        if (!dict.ContainsKey(item[0])) {
                            dict.Add(item[0], "");
                        }
                        else {
                            dict[item[0]] = "";
                        }
                    }
                }

                return dict;
            }
            catch (Exception ex) {
                Qdb.Error(ex.Message, QDebugErrorType.Error, "Decode_Simple");
                return new Dictionary<string, string>();
            }
        }
    }
}