﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QFrameworkOne.Module;

namespace QFrameworkOne.Module {
    /// <summary>
    /// 生成随机密码
    /// </summary>
    public class GenerateSimplePassword : QModuleBase, IQMoudule {
        string IQMoudule.Module_Name => "GenerateSimplePassword";

        string IQMoudule.Module_Author => "DealiAxy";

        string IQMoudule.Module_Mail => "admin@deali.cn";

        string IQMoudule.Module_Website => "http://blog.deali.cn";

        string IQMoudule.Module_Description => "生成简单随机密码";

        QComponentVersion IQMoudule.Module_Version => new QComponentVersion()
            {Major = 1, Minor = 2, Update = 8, Label = QComponentVersionLabel.Alpha};

        /// <summary>
        /// 符号表
        /// </summary>
        public static string ListAllChars = "";

        /// <summary>
        /// 0-9 数字列表
        /// </summary>
        public static string ListNum = "0123456789";

        /// <summary>
        /// 小写字母列表
        /// </summary>
        public static string ListLetterLower = "abcdefghijklmnopqrstuvwxyz";

        /// <summary>
        /// 大写字母列表
        /// </summary>
        public static string ListLetterUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        /// <summary>
        /// 符号列表
        /// </summary>
        public static string ListSymbol = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";

        /// <summary>
        /// 初始化字符列表
        /// </summary>
        static GenerateSimplePassword() {
            ListAllChars += ListNum + ListLetterLower + ListLetterUpper + ListSymbol;

            //for (int i = 33; i <= 47; i++)
            //{
            //    ListAllChars.Add((char)i);
            //}
            //for (int i = 58; i <= 64; i++)
            //{
            //    ListAllChars.Add((char)i);
            //}
            //for (int i = 91; i <= 96; i++)
            //{
            //    ListAllChars.Add((char)i);
            //}
            //for (int i = 123; i <= 126; i++)
            //{
            //    ListAllChars.Add((char)i);
            //}
        }

        ///<summary>
        ///生成随机密码
        ///</summary>
        ///<param name="length">密码长度</param>
        ///<param name="useNum">是否包含数字，默认为包含</param>
        ///<param name="useLow">是否包含小写字母，默认为包含</param>
        ///<param name="useUpp">是否包含大写字母，默认为包含</param>
        ///<param name="useSpe">是否包含特殊字符，默认为不包含</param>
        ///<param name="custom">自定义字符，直接输入要包含的字符列表</param>
        ///<returns>指定长度的随机密码字符串</returns>
        public static string RandomPassword(int length, bool useNum, bool useLow, bool useUpp, bool useSpe = false,
            string custom = "") {
            byte[] b = new byte[4];
            new System.Security.Cryptography.RNGCryptoServiceProvider().GetBytes(b);
            int seed = BitConverter.ToInt32(b, 0);
            Random r = new Random(seed);
            string s = null, str = custom;
            if (useNum == true) {
                str += "0123456789";
            }

            if (useLow == true) {
                str += "abcdefghijklmnopqrstuvwxyz";
            }

            if (useUpp == true) {
                str += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            }

            if (useSpe == true) {
                str += "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
            }

            for (int i = 0; i < length; i++) {
                s += str.Substring(r.Next(0, str.Length - 1), 1);
            }

            return s;
        }

        /// <summary>
        /// 生成简单纯数字密码
        /// </summary>
        /// <param name="count">密码位数</param>
        public static string SimpleNumber(int count) {
            int seed = DateTime.Now.Millisecond;
            Console.WriteLine("Seed={0}", seed);
            Random rnd = new Random(seed);
            string result = "";
            if (count > 0) {
                double time1 = DateTime.Now.TimeOfDay.TotalMilliseconds;
                for (int i = 1; i <= count; i++) {
                    result += rnd.Next(0, 10).ToString();
                }

                double time2 = DateTime.Now.TimeOfDay.TotalMilliseconds;
                Console.WriteLine("Use time {0} millisecond.", time2 - time1);
                return result;
            }
            else
                return "";
        }

        /// <summary>
        /// 生成简单纯字母密码（小写字母）
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public static string SimpleAlphabet(int count) {
            string result = "";

            if (count > 0) {
                double time1 = DateTime.Now.TimeOfDay.TotalMilliseconds;
                for (int i = 1; i <= count; i++) {
                    result += RandomAlphabet();
                }

                double time2 = DateTime.Now.TimeOfDay.TotalMilliseconds;
                Console.WriteLine("Use time {0} millisecond.", time2 - time1);
                return result;
            }
            else
                return "";
        }

        /// <summary>
        /// 生成简单纯符号密码
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public static string SimpleSymbol(int count) {
            string result = "";

            if (count > 0) {
                double time1 = DateTime.Now.TimeOfDay.TotalMilliseconds;
                for (int i = 1; i <= count; i++) {
                    result += RandomSymbol();
                }

                double time2 = DateTime.Now.TimeOfDay.TotalMilliseconds;
                Console.WriteLine("Use time {0} millisecond.", time2 - time1);
                return result;
            }
            else
                return "";
        }

        /// <summary>
        /// 取随机字母
        /// </summary>
        /// <returns></returns>
        private static char RandomAlphabet() {
            byte[] b = new byte[4];
            new System.Security.Cryptography.RNGCryptoServiceProvider().GetBytes(b);
            int seed = BitConverter.ToInt32(b, 0);
            Console.WriteLine("Seed={0}", seed);
            Random rnd = new Random(seed);
            return (char) (rnd.Next((int) 'a', (int) 'z' + 1));
        }

        /// <summary>
        /// 取随机字符
        /// </summary>
        /// <returns></returns>
        private static char RandomSymbol() {
            byte[] b = new byte[4];
            new System.Security.Cryptography.RNGCryptoServiceProvider().GetBytes(b);
            int seed = BitConverter.ToInt32(b, 0);
            Console.WriteLine("Seed={0}", seed);
            Random rnd = new Random(seed);
            return ListSymbol[rnd.Next(ListSymbol.Length)];
        }
    }
}