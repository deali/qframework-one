﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using QFrameworkOne.Diagnostics;

namespace QFrameworkOne.Module {
    /// <summary>
    /// 
    /// </summary>
    public class QNetwork : IQMoudule {
        string IQMoudule.Module_Name => throw new NotImplementedException();

        string IQMoudule.Module_Author => throw new NotImplementedException();

        string IQMoudule.Module_Mail => throw new NotImplementedException();

        string IQMoudule.Module_Website => throw new NotImplementedException();

        string IQMoudule.Module_Description => throw new NotImplementedException();

        QComponentVersion IQMoudule.Module_Version => throw new NotImplementedException();

        static QDebug Qdb = new QDebug() {
            CodeLocation = "QFrameworkOne.Module.QNetwork",
            ConsoleOutput = true,
        };

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetIPAddress() {
            string s_Url = "http://cloud.deali.cn/getip.php";
            string s_Src = "";
            using (WebClient client = new WebClient()) {
                client.Encoding = System.Text.Encoding.Default;
                try {
                    s_Src = client.DownloadString(s_Url);
                    return s_Src;
                }
                catch (Exception ex) {
                    Qdb.Error("下载IP地址网页失败。" + ex.Message, QDebugErrorType.Fatal, "GetIPString");
                    return Environment.UserDomainName;
                }
            }
        }
    }
}