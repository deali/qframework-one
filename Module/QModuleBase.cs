﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace QFrameworkOne.Module {
    /// <summary>
    /// QModule 基类 以后写QMod都要从这个类派生
    /// 已停用
    /// </summary>
    public class QModuleBase {
        private static string s_ModuleAuthor;
        private static string s_AuthorMail;
        private static string s_Website;
        private static string s_ModuleName;
        private static string s_ModuleDescription;
        private static QComponentVersion v_ModuleVersion = new QComponentVersion();

        /// <summary>
        /// 模块名称
        /// </summary>
        public static string Name {
            get { return s_ModuleName; }
            set { s_ModuleName = value; }
        }

        /// <summary>
        /// 模块作者
        /// </summary>
        public static string Author {
            get { return s_ModuleAuthor; }
            set { s_ModuleAuthor = value; }
        }

        /// <summary>
        /// 作者邮箱
        /// </summary>
        public static string AuthorMail {
            get { return s_AuthorMail; }
            set { s_AuthorMail = value; }
        }

        /// <summary>
        /// 相关网站
        /// </summary>
        public static string Website {
            get { return s_Website; }
            set { s_Website = value; }
        }

        /// <summary>
        /// 模块说明
        /// </summary>
        public static string Description {
            get { return s_ModuleDescription; }
            set { s_ModuleDescription = value; }
        }

        /// <summary>
        /// 模块版本，字符串，例如”Beta“、”Alpha“
        /// </summary>
        public static string VersionStr {
            get { return Version.ToString(); }
        }

        /// <summary>
        /// 模块版本，QModuleVersion 类型，分为主版本号、次版本号和更新号
        /// </summary>
        public static QComponentVersion Version {
            get { return v_ModuleVersion; }
            set { v_ModuleVersion = value; }
        }
    }

    /// <summary>
    /// QModule接口，所有模块都必须实现该接口
    /// </summary>
    internal interface IQMoudule {
        /// <summary>
        /// 名称
        /// </summary>
        string Module_Name { get; }

        /// <summary>
        /// 开发者
        /// </summary>
        string Module_Author { get; }

        /// <summary>
        /// 开发者邮箱
        /// </summary>
        string Module_Mail { get; }

        /// <summary>
        /// 网站
        /// </summary>
        string Module_Website { get; }

        /// <summary>
        /// 说明
        /// </summary>
        string Module_Description { get; }

        /// <summary>
        /// 版本
        /// </summary>
        QComponentVersion Module_Version { get; }
    }
}