﻿using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace QFrameworkOne.Module {
    /// <summary>
    /// 各类加密方法的封装模块
    /// </summary>
    public class EncryptHelper : IQMoudule {
        string IQMoudule.Module_Name => "EncryptHelper";

        string IQMoudule.Module_Author => "WeiHanLi";

        string IQMoudule.Module_Mail => "";

        string IQMoudule.Module_Website => @"http://www.cnblogs.com/weihanli/p/encryptionViaCSharp.html";

        string IQMoudule.Module_Description => "从cnblog上拿过来的常用加密方法模块";

        QComponentVersion IQMoudule.Module_Version => new QComponentVersion() {
            Major = 1,
            Label = QComponentVersionLabel.Beta
        };

        /// <summary>
        /// 获取某个哈希算法对应下的哈希值
        /// </summary>
        /// <param name="sourceString">源字符串</param>
        /// <param name="algorithm">哈希算法</param>
        /// <returns>经过计算的哈希值</returns>
        private static string GetHash(string sourceString, HashAlgorithm algorithm) {
            byte[] sourceBytes = Encoding.UTF8.GetBytes(sourceString);
            byte[] result = algorithm.ComputeHash(sourceBytes);
            algorithm.Clear();
            StringBuilder sb = new StringBuilder(32);
            for (int i = 0; i < result.Length; i++) {
                sb.Append(result[i].ToString("X2"));
            }

            return sb.ToString();
        }

        /// <summary>
        /// 获取MD5值
        /// </summary>
        /// <param name="sourceString">源字符串</param>
        /// <returns>MD5值</returns>
        public static string GetMD5(string sourceString) {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            return GetHash(sourceString, md5);
        }

        /// <summary>
        /// 获取SHA1值
        /// </summary>
        /// <param name="sourceString">源字符串</param>
        /// <returns>SHA1值</returns>
        public static string GetSHA1(string sourceString) {
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            return GetHash(sourceString, sha1);
        }

        /// <summary>
        /// 获取SHA256值
        /// </summary>
        /// <param name="sourceString">源字符串</param>
        /// <returns>SHA256值</returns>
        public static string GetSHA256(string sourceString) {
            SHA256 sha256 = SHA256.Create();
            return GetHash(sourceString, sha256);
        }

        /// <summary>
        /// 获取SHA384值
        /// </summary>
        /// <param name="sourceString">源字符串</param>
        /// <returns>SHA384值</returns>
        public static string GetSHA384(string sourceString) {
            SHA384 sha384 = SHA384.Create();
            return GetHash(sourceString, sha384);
        }

        /// <summary>
        /// 获取SHA512值
        /// </summary>
        /// <param name="sourceString">源字符串</param>
        /// <returns>SHA512值</returns>
        public static string GetSHA512(string sourceString) {
            SHA512 sha512 = SHA512.Create();
            return GetHash(sourceString, sha512);
        }

        /// <summary>
        /// 获取文件的Base64编码
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string GetFileBase64String(string filePath) {
            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                using (BinaryReader reader = new BinaryReader(fs)) {
                    try {
                        return GetBase64String(reader.ReadBytes((int) fs.Length));
                    }
                    catch (System.Exception ex) {
                        throw ex;
                    }
                }
            }
        }

        /// <summary>
        /// 使用Base64加密字符串
        /// </summary>
        /// <param name="sourceString"></param>
        /// <returns></returns>
        public static string GetBase64String(string sourceString) {
            byte[] buffer = Encoding.UTF8.GetBytes(sourceString);
            return GetBase64String(buffer);
        }

        /// <summary>
        /// 使用Base64加密字符串
        /// </summary>
        /// <param name="sourceString"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static string GetBase64String(string sourceString, Encoding encoding) {
            byte[] buffer = encoding.GetBytes(sourceString);
            return GetBase64String(buffer);
        }

        /// <summary>
        /// 使用Base64加密字符串
        /// </summary>
        /// <param name="sourceBytes"></param>
        /// <returns></returns>
        public static string GetBase64String(byte[] sourceBytes) {
            string base64String = System.Convert.ToBase64String(sourceBytes);
            return base64String;
        }
    }
}