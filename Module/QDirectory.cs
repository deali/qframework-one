﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using QFrameworkOne.Diagnostics;

namespace QFrameworkOne.Module {
    /// <summary>
    /// 文件夹操作模块
    /// </summary>
    public class QDirectory : IQMoudule {
        string IQMoudule.Module_Name => "QDirectory";

        string IQMoudule.Module_Author => "DealiAxy";

        string IQMoudule.Module_Mail => "admin@deali.cn";

        string IQMoudule.Module_Website => "http://blog.deali.cn";

        string IQMoudule.Module_Description => "文件夹操作模块";

        QComponentVersion IQMoudule.Module_Version => new QComponentVersion()
            {Major = 1, Minor = 1, Update = 1, Label = QComponentVersionLabel.Alpha};

        static QDebug Qdb = new QDebug() {
            CodeLocation = "QFrameworkOne.Module.QDirectory",
            FileOutput = true,
            ConsoleOutput = true
        };

        /// <summary>
        /// 复制文件夹
        /// </summary>
        /// <param name="fromDir"></param>
        /// <param name="toDir"></param>
        public static void Copy(string fromDir, string toDir) {
            if (!Directory.Exists(fromDir))
                return;

            if (!Directory.Exists(toDir)) {
                Directory.CreateDirectory(toDir);
            }

            string[] files = Directory.GetFiles(fromDir);
            foreach (string formFileName in files) {
                string fileName = Path.GetFileName(formFileName);
                string toFileName = Path.Combine(toDir, fileName);
                File.Copy(formFileName, toFileName);
            }

            string[] fromDirs = Directory.GetDirectories(fromDir);
            foreach (string fromDirName in fromDirs) {
                string dirName = Path.GetFileName(fromDirName);
                string toDirName = Path.Combine(toDir, dirName);
                Copy(fromDirName, toDirName);
            }
        }

        /// <summary>
        /// 创建文件夹，如存在不建立
        /// </summary>
        /// <param name="path"></param>
        /// <returns>返回路径</returns>
        public static string Create(string path) {
            try {
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
            }
            catch (Exception ex) {
                Qdb.Error(ex.Message);
            }

            return path;
        }

        /// <summary>
        /// 移动文件夹
        /// </summary>
        /// <param name="fromDir"></param>
        /// <param name="toDir"></param>
        public static void Move(string fromDir, string toDir) {
            if (!Directory.Exists(fromDir))
                return;

            Copy(fromDir, toDir);
            Directory.Delete(fromDir, true);
        }
    }
}