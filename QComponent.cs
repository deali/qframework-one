﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QFrameworkOne.Diagnostics;

namespace QFrameworkOne {
    /// <summary>
    /// QComponentVersion 版本标签枚举
    /// </summary>
    public enum QComponentVersionLabel {
        /// <summary>
        /// Alpha
        /// </summary>
        Alpha = 0,

        /// <summary>
        /// Beta
        /// </summary>
        Beta = 1,

        /// <summary>
        /// RC
        /// </summary>
        RC = 2,

        /// <summary>
        /// Release
        /// </summary>
        Release = 3,

        /// <summary>
        /// 最终版本，不再迭代
        /// </summary>
        Final = 4
    }

    /// <summary>
    /// QCoomponent 组件通用版本类
    /// </summary>
    public class QComponentVersion {
        /// <summary>
        /// 主版本号
        /// </summary>
        public int Major = 0;

        /// <summary>
        /// 次版本号
        /// </summary>
        public int Minor = 0;

        /// <summary>
        /// 版本更新号
        /// </summary>
        public int Update = 0;

        /// <summary>
        /// 版本标签 如 Alpha Beta RC Release
        /// </summary>
        public QComponentVersionLabel Label = QComponentVersionLabel.Alpha;

        private static QDebug Qdb = new QDebug() {CodeLocation = "QFrameworkOne.QComponentVersion"};

        /// <summary>
        /// 默认构造函数
        /// </summary>
        public QComponentVersion() {
        }

        /// <summary>
        /// 获取字符串形式版本号，重写 object.ToString()
        /// </summary>
        /// <returns>版本号</returns>
        public override string ToString() {
            return string.Format("{0}.{1}.{2} {3}", Major, Minor, Update, Label.ToString());
        }

        /// <summary>
        /// 将字符串形式的版本号转换为QApplicationVersion格式
        /// </summary>
        /// <param name="s_VersionStr">版本号字符串 1.0.0 Beta 形式</param>
        /// <returns></returns>
        public static QComponentVersion Parse(string s_VersionStr) {
            s_VersionStr = s_VersionStr.Trim();

            QComponentVersion v_Ver = new QComponentVersion();
            string[] s_Vers1, s_Vers2;
            string s_Ver;

            if (s_VersionStr.IndexOf(" ") < 0) {
                v_Ver.Label = QComponentVersionLabel.Alpha;
                s_Ver = s_VersionStr;
            }
            else {
                s_Vers1 = s_VersionStr.Split(' ');
                try {
                    v_Ver.Label = (QComponentVersionLabel) Enum.Parse(typeof(QComponentVersionLabel), s_Vers1[1], true);
                }
                catch (Exception ex) {
                    v_Ver.Label = QComponentVersionLabel.Alpha;
                    Qdb.Error(
                        "QComponentVersion.Parse(string s_VersionStr)->字符串版本Label转换为QComponentVersionLabel枚举类型失败，请检查标签字符串是否有误 ex.Message=" +
                        ex.Message, QDebugErrorType.Fatal, "Parse");
                }

                s_Ver = s_Vers1[0];
            }

            if (s_VersionStr.IndexOf('.') < 0)
                return v_Ver;

            s_Vers2 = s_Ver.Split('.');

            switch (s_Vers2.Length) {
                case 0:
                    return v_Ver;
                case 1:
                    v_Ver.Major = int.Parse(s_Vers2[0]);
                    break;
                case 2:
                    v_Ver.Major = int.Parse(s_Vers2[0]);
                    v_Ver.Minor = int.Parse(s_Vers2[1]);
                    break;
                case 3:
                    v_Ver.Major = int.Parse(s_Vers2[0]);
                    v_Ver.Minor = int.Parse(s_Vers2[1]);
                    v_Ver.Update = int.Parse(s_Vers2[2]);
                    break;
            }

            return v_Ver;
        }
    }

    /// <summary>
    /// QComponent 组件基类
    /// </summary>
    public class QComponentBase {
        /// <summary>
        /// 组件代码版本
        /// </summary>
        public QComponentVersion CodeVersion = new QComponentVersion();

        /// <summary>
        /// QFramework版本
        /// </summary>
        public QComponentVersion QFrameworkVersion = QFrameworkStatic.Version;

        /// <summary>
        /// 代码位置描述，最好包含命名空间，方便QDebug对象记录调试日志
        /// </summary>
        public string CodeLocation = "";
    }
}